#!/usr/bin/env python

import requests
import json
import os

print("Start")

f = "./database.json"

data = json.load(open(f))

setslist = [["id","name"]]

for card in data:

    card_s  = card['set']
    card_sn = card['set_name']

    ids = [elem[0] for elem in setslist]

    if card_s not in ids:
        setslist.append([card_s,card_sn])


setslist.pop(0)

setslist.sort(key=lambda x: x[1])

fn = "./sets_dict.txt"

os.remove(fn) if os.path.exists(fn) else None

f = open(fn,"w")

for s in setslist:
    f.write(s[0] + ";;" + s[1] + "\n")

f.close()

print("Done writing")
