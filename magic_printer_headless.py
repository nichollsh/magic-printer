#!/usr/bin/env python

print("Start")

import os
import glob
import sys
import shutil
from PIL import Image
import requests
import json
from datetime import datetime
from dateutil.relativedelta import relativedelta

# Variables


deckPath    = "/home/harrison/.local/share/Cockatrice/Cockatrice/decks/W_halvar.txt"
dbPath      = "/home/harrison/programming/magic-printer/database.json"
# Database downloaded from: https://scryfall.com/docs/api/bulk-data/all
# Use the one called "Default Cards"

# bd_info = json.loads(requests.get("https://api.scryfall.com/bulk-data").content.decode('UTF-8'))
# bd_url  = bd_info["data"][2]["download_uri"]
# print("Database update URL: " + str(bd_url))


use_prefer   = True

outputW     = 1765
outputH     = 1200
cardW       = 723
cardH       = 1011

offsetX     = 100
paddingX    = 100
offsetY     = 50

bgColor     = (191, 207, 219)

verbose = True
overwrite = True
quality = 3             # 0 = Small ; 1 = Normal ; 2 = Large ; 3 = PNG


# Handle parameters

if (len(sys.argv) > 1):
    deckPath = sys.argv[1]

quality = ['small','normal','large','png'][quality]

print("Using cards at " + quality + " quality")

# Configure directories

outputDir = os.getcwd() + "/output/"
print("Output dir: '" + outputDir + "'")

cacheDir = os.getcwd() + "/cache/"
print("Cache dir:  '" + cacheDir + "'")

for f in glob.glob(cacheDir + "*.part"):
    os.remove(f)


# Load database

print("Database:   '" + str(dbPath) + "'")

db = json.load(open(dbPath))

# Make array of all cards to gather

print("Deck:       '" + deckPath + "'")

deck = open(deckPath, 'r') # Load deck
deckLines = deck.readlines() # Read lines as elements of array
deck.close() # Close file
deckLines = [s.replace('\n','') for s in deckLines] # Remove newline chars
deckLines = [s.replace(' // ','') for s in deckLines] #corrects for split cards such as Wear // Tear

cards_all = []          # Array of card names
cards_quantity = []     # How many of each card
for l in deckLines: # for each line
    if (l == ""):
        continue
    split = l.split(" ",1)
    num = int(split[0])
    cards_all.append(split[1])
    cards_quantity.append(int(split[0]))

image_names = []

print("Collecting cards")

# Function for scoring cards
def calcScore(card):

    set_excl         = ['afr','sta','stx','hml','prwk','sld','jou','mma']
    score_set_excl   = -3

    set_pref         = ['eld','2xm','m12','e02']
    score_set_pref   = 3

    score_promo      = 0
    score_borderless = 1
    score_reprint    = 1
    score_badimg     = -1

    old_years        = 11
    score_old        = -1

    s = 0

    if (card['set'] in set_excl):
        s = s + score_set_excl
    
    if (card['set'] in set_pref):
        s = s + score_set_pref

    if (card['promo'] == True):
        s = s + score_promo

    if (card['border_color'] == "borderless"):
        s = s + score_borderless

    if (card['reprint'] == True):
        s = s + score_reprint

    if (card['image_status'] != "highres_scan"):
        s = s + score_badimg

    date_printed   = datetime.strptime(card["released_at"], "%Y-%m-%d").date()
    date_threshold = (datetime.now() - relativedelta(years=old_years)).date()
    if (date_printed < date_threshold):
        s = s + score_old

    return s


# Function to download cards if not already downloaded
def downloadCard(name,url):
    fname = cacheDir + name + ".png"

    if os.path.isfile(fname) and not overwrite:
        if verbose: print("     Extant")
    else:
        open(fname + ".part", 'wb').write(requests.get(url, allow_redirects=True).content)
        shutil.move(fname + ".part",fname)
        if verbose: print("     Downloaded")



# Create cache dir if it doesn't exist
if not (os.path.isdir(cacheDir)): os.mkdir(cacheDir)

# Loop over cards, locate in database, rank, and download them
for i,target_name in enumerate(cards_all):

    match_score = -9999
    match_uuid = "empty"
    match_url1 = "empty"
    match_url2 = "empty"
    match_n1 = "empty"
    match_n2 = "empty"
    match_set = "empty"

    # Loop through database for this card
    for card in db:
        name = card['name']

        # Check if double sided
        if " // " in name:
            n1 = name.split(" // ")[0]
            n2 = name.split(" // ")[1]
        else:
            n1 = name
            n2 = "empty"

        # Adventure cards only have one side, despite using "//" in their name
        if (card['layout'] == 'adventure'):
            n2 = "empty"

        # Not legal but very pretty
        if (card['layout'] == 'art_series'):
            continue

        # If single sided
        if (n1.lower() == target_name.lower()) and (n2 == "empty"):

            uuid = card['id']

            if (use_prefer == False) and (match > 0):
                break

            new_score = calcScore(card)
            if (new_score > match_score):
                match_score = new_score
                match_uuid = uuid
                match_url1 = card['image_uris'][quality]
                match_n1 = n1
                match_set = card['set']

        # If double sided
        elif (n1.lower() == target_name.lower()) or (n2.lower() == target_name.lower()):

            uuid = card['id']

            if (use_prefer == False) and (match > 0):
                break

            new_score = calcScore(card)
            if (new_score > match_score):
                match_score = new_score
                match_uuid = uuid
                match_url1 = card['card_faces'][0]['image_uris'][quality]
                match_n1 = n1
                match_url2 = card['card_faces'][1]['image_uris'][quality]
                match_n2 = n2
                match_set = card['set']

        # else pass
    
    if (match_score == -9999):
        print("Card '" + target_name + "' not found in database.")
        print("Exiting")
        exit()
    else:
        
        if (match_url2 == "empty"):
            if verbose:
                print(str(i).zfill(3),end='')
                print("  Front:    " + match_n1)
                print("     Quantity: " + str(cards_quantity[i]))
                print("     Goodness: " + str(match_score))
                print("     Set:      " + str(match_set))
            for _ in range(cards_quantity[i]):
                image_names.append(quality + "_" +  match_uuid)
            downloadCard(quality + "_" + match_uuid,match_url1)
        else:
            if verbose:
                print(str(i).zfill(3),end='')
                print("  Front:    " + match_n1)
                print("     Back:     " + match_n2)
                print("     Quantity: " + str(cards_quantity[i]))
                print("     Goodness: " + str(match_score))
                print("     Set:      " + str(match_set))
            for _ in range(cards_quantity[i]):
                image_names.append(quality + "_" + match_uuid + "_front")
                image_names.append(quality + "_" + match_uuid + "_back")
            downloadCard(quality + "_" + match_uuid +"_front",match_url1)
            downloadCard(quality + "_" + match_uuid +"_back",match_url2)


# Create composites

total_cards = len(image_names)

print("Generating composites from " + str(total_cards) + " cards")

if (os.path.isdir(outputDir)): 
    shutil.rmtree(outputDir,ignore_errors=True)
os.mkdir(outputDir)

i = 0
while (i < total_cards):

    resample = Image.BICUBIC
    comp = Image.new('RGBA', (outputW, outputH), color = bgColor)

    print(str(i).zfill(3),end='')

    # Card 1
    c1 = cacheDir + image_names[i] + ".png"
    print("  " + image_names[i])
    img1 = Image.open(c1).resize((cardW,cardH),resample=resample)
    if (quality == 'png'):
        comp.alpha_composite(img1.convert("RGBA"),dest=(offsetX,offsetY))
    else:
        comp.paste(img1, (offsetX,offsetY))
    
    # Card 2
    if (i+1 != total_cards):
        c2 = cacheDir + image_names[i+1] + ".png"
        print("     " + image_names[i+1])
        img2 = Image.open(c2).resize((cardW,cardH),resample=resample)
        if (quality == 'png'):
            comp.alpha_composite(img2.convert("RGBA"),dest=(offsetX+cardW+paddingX,offsetY))
        else:
            comp.paste(img2, (offsetX+cardW+paddingX,offsetY))

    # Save composite
    comp.save(outputDir + str(i) + ".png")

    i = i + 2


print("Done")
