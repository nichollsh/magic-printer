#!/usr/bin/env python


import requests
import json
import os

f = "./default.json"

data = json.load(open(f))

# target_name = "Fury Sliver"
target_name = "Sword of Fire and Ice"

c = 0
found = False

def printCard(fname, url):
    open(fname+".png", 'wb').write(requests.get(url, allow_redirects=True).content)

for card in data:
    name = card['name']

    if " // " in name:
        n1 = name.split(" // ")[0]
        n2 = name.split(" // ")[1]
    else:
        n1 = name
        n2 = "empty"


    if (n1 == target_name) and (n2 == "empty"):
        print("Found target " + n1)

        uuid = card['id']
        url  = card['image_uris']['large']
        printCard(uuid,url)

        found = True
        break

    elif (n1 == target_name) and (n2 != "empty"):
        print("Found target: " + n1)
        print("Other side:   " + n2)

        uuid = card['id']

        print(card)

        url = card['card_faces'][0]['image_uris']['large']
        printCard(uuid+"_front",url)

        url = card['card_faces'][1]['image_uris']['large']
        printCard(uuid+"_back",url)

        found = True
        break
    


if not found:
    print("Card not found in database")
    exit()

print("Done")

# name = target_card['name']
# uuid = target_card['id']
# url  = target_card['image_uris']['large']
