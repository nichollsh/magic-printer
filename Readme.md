# Magic Printer
Software that generates proxy Magic: The Gathering cards, by Harrison Nicholls  

## Software prerequisites
* Linux, macOS, Windows
* An internet connection
* Python 3
* Python 3 Libraries
  * Tkinter
  * Pillow
  * JSON

## Running the software
Simply run `magic_printer_gui.py`. This can be done by double clicking the file in your file manager, or from a terminal environment.

## Overview
This software takes a previously created MTG deck file as input, and outputs images with cards on that can be easily printed.  
To do this, it takes you through a process similar to an installation wizard...  
1. Load deck file into software and load/update card database.  
2. Configure the software to select cards based on your requirements. If you love borderless cards, it will print those. If you hate *Adventures in the Forgotten Realms*, it will avoid printing cards from that set. There are many options to choose from.  
3. Cards will be selected from the Scryfall database using your requirements.
4. You will be given the option to adjust the best-matching cards, if you aren't happy with what this software has picked out for you.  
5. Configure settings such as output folder and printer paper size/dpi.  
6. Create card 'composites' and write the files to your computer's hard drive.  
I recommend using an online photo-printing service such as [FreePrints](https://www.freeprintsapp.co.uk/) to print the card composites. (Not sponsored).


## Credits and disclaimers

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.

* Card images and data are sourced from Scryfall [(https://scryfall.com)](https://scryfall.com)

* MTG cards presented inside this software are unofficial Fan Content permitted under the Fan Content Policy. Not approved/endorsed by Wizards. Portions of the materials used are property of Wizards of the Coast. © Wizards of the Coast LLC. This software is not produced by, endorsed by, supported by, or affiliated with Wizards of the Coast. See the WotC website for more information: [https://company.wizards.com/en/legal/fancontentpolicy](https://company.wizards.com/en/legal/fancontentpolicy).